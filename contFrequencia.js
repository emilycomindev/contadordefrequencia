

document.getElementById("countButton").onclick = function() { 

   let typedText = document.getElementById("textInput").value;

   //o código tem que mostrar no html as funções abaixo?

   typedText = typedText.toLowerCase(); 
   typedText = typedText.replace(/[^a-z'\s]+/g, "");
   words = typedText.split(/\s/);

   const letterCounts = {

    }
   const wordCounts = {

    }

   for (let i = 0; i < typedText.length; i++) {
      
      currentLetter = typedText[i];

      if (letterCounts[currentLetter] === undefined) {
         letterCounts[currentLetter] = 1; 
      }
       else { 
          letterCounts[currentLetter]++; 
      }
   } 

   for (let i = 0; i < words.length; i++) {

      currentWord = words[i];

      if(wordCounts[currentWord] === undefined){
         wordCounts[currentWord] = 1;
      }
      else{
         wordCounts[currentWord]++;
      }
   }

   for (let letter in letterCounts) { 
      
      const spanLetters = document.createElement("span"); 
      const textContent = document.createTextNode('"' + letter + " \": " + letterCounts[letter] + ", "); 

      spanLetters.appendChild(textContent); 
      document.getElementById("lettersDiv").appendChild(spanLetters); 
   }
   
   for (let words in wordCounts) {
      const spanWords = document.createElement("span"); 
      const wordContent = document.createTextNode('"' + words + "\: " + wordCounts[words] + ", " )

      spanWords.appendChild(wordContent);
      document.getElementById("wordsDiv").appendChild(spanWords);  
   } 

}